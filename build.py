#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os import path

from conan.packager import ConanMultiPackager
from conans import tools

def main():
    builder = ConanMultiPackager(
                    reference=get_package_reference(),
                    build_types=["Release"],
                    build_policy="outdated")

    builder.add_common_builds()
    builder.run()

#####################################################################

def get_package_reference():
    pkg_info = get_package_info()

    return "{}/{}".format(pkg_info[0], pkg_info[1])

def get_package_info():
    import inspect

    conanfile = import_module(path.abspath('conanfile.py'))
    module_members = inspect.getmembers(conanfile, is_recipe)
    print("module_members({}): {}".format(conanfile, module_members))

    for name, member in module_members:
         if member.__module__ == conanfile.__name__:
             return (member.name, member.version)

    return None

def import_module(module_path):
    import sys
    import imp

    # Change module name to avoid conflict with ConanMultiPackager
    name = 'temp_' + path.splitext(path.basename(module_path))[0]

    sys.dont_write_bytecode = True
    loaded = imp.load_source(name, module_path)
    sys.dont_write_bytecode = False

    return loaded

def is_recipe(member):
    import inspect
    from conans import ConanFile

    return inspect.isclass(member) and issubclass(member, ConanFile)


#####################################################################

if __name__ == "__main__":
    main()
