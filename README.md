# hxcpp Conan Generator

**master ·** [![pipeline status](https://gitlab.com/no-face/hxcpp-conangenerator/badges/master/pipeline.svg)](https://gitlab.com/no-face/hxcpp-conangenerator/commits/master)
[![Build status](https://ci.appveyor.com/api/projects/status/k5v3tbmvru0twypt/branch/master?svg=true)](https://ci.appveyor.com/project/no-face/hxcpp-conangenerator/branch/master)
**· dev ·** [![pipeline status](https://gitlab.com/no-face/hxcpp-conangenerator/badges/dev/pipeline.svg)](https://gitlab.com/no-face/hxcpp-conangenerator/commits/dev)
[![Build status](https://ci.appveyor.com/api/projects/status/k5v3tbmvru0twypt/branch/dev?svg=true)](https://ci.appveyor.com/project/no-face/hxcpp-conangenerator/branch/dev)
**· conan ·** [![Download](https://api.bintray.com/packages/noface/conan/HxcppGenerator%3Anoface/images/download.svg) ](https://bintray.com/noface/conan/HxcppGenerator%3Anoface/_latestVersion)

A [Conan][conan_home] generator to use with [Haxe][haxe_home]
cpp target ([hxcpp][hxcpp_home]).

See also the [ConanBuilder][conan_builder_repo] project
(*in development*), to integrate automatically conan
dependencies into haxe (hxcpp) projects.

[conan_home]: https://www.conan.io/
[haxe_home]: https://haxe.org
[hxcpp_home]: https://github.com/HaxeFoundation/hxcpp
[conan_builder_repo]: https://gitlab.com/no-face/conan-builder

## How to use

### 1. Add the remote to download the generator recipe

Currently, this recipe is not at [conan-center][conan_center_page],
so you need to add this repository to your conan client:

`https://api.bintray.com/conan/noface/conan`

Example:

`conan remote noface https://api.bintray.com/conan/noface/conan`


[conan_center_page]: https://bintray.com/conan/conan-center

### 2. Add the generator to the conanfile

To use this generator, add it as a build requirement, and list it in the
generators section.

Example:

```python
from conans import ConanFile

class Recipe(ConanFile):
    ...
    generators = "Hxcpp"

    build_requires = "HxcppGenerator/0.1.4@noface/testing"

    requires = <your conan requirements>
    ...
```

### 3. Install the recipe

Run:

`conan install <path>`

Where the `<path>` can be the conanfile path or the path
to the directory where the conanfile is.


### 4. Include the generated file into Haxe(hxcpp) build

After the `conan install` command, a file `conanbuildinfo_hxcpp.xml`
will be generated with the configuration needed to apply
the conan requirements to the [`haxe` target][haxe_target].

The generated file needs to be included into the build.
See the [haxe documentation][haxe_cpp_target] for more
details.

You can include this file, using the `buildXml` metadata.
Example:

```
@:buildXml("<include name='path/to/conanbuildinfo_hxcpp.xml'/>")
extern class MyClass{
  ...
}
```

Or if you already are using a custom `Build.xml`, you can
include the file directly:

```
<xml>
...
<include name='path/to/conanbuildinfo_hxcpp.xml'/>
...
</xml>
```

**Warning:** the path to include a file in hxcpp is relative
to the `Build.xml` file. So, it may be trick to include the file.
One possibility is pass a define that contains the path to the file
(ex.: `<include name='${CONANFILE_DIR}/conanbuildinfo_hxcpp.xml'/>`),
or copy the generated file into the build directory and reference it
directly (ex.: `<include name='conanbuildinfo_hxcpp.xml'/>`).


[haxe_cpp_target]:https://haxe.org/manual/target-cpp.html
[haxe_target]: https://haxe.org/manual/hxcpp/docs/build_xml/HaxeTarget.html


## Notes

### Cross (architecture) compiling

> **Warning:** Currently cross compile is not fully tested/supported.

When building from an x86 (32bit <-> 64 bit) architecture to another you may
need to set the haxe flags HXCPP_M32 or HXCPP_M64, for 32 bits or 64 bits,
respectively.

This issue was detected when building to Visual Studio 64 bits (it fails, due to
`__readgsqword` symbol not found). Despite we set this defines in the
`conanbuildinfo_hxcpp.xml` file, it apparently requires the file to be set before.

However, if you are running the haxe command from a conanfile, this may not be a
problem, because we set those flags in the environment.

## License

This generator is licensed under the [Unlicense][unlicense_home] terms.
See the UNLICENSE.md file.

[unlicense_home]: https://unlicense.org/
