#!/usr/bin/env python
# -*- coding: utf-8 -*-

class DepsInfo(object):
    def __init__(self, **kwargs):
        self.compilerflags = kwargs.get('compilerflags', [])
        self.linkflags     = kwargs.get('linkflags', [])
        self.libpaths      = kwargs.get('libpaths', [])
        self.libs          = kwargs.get('libs', [])
        self.rpath_flags   = kwargs.get('rpath_flags', [])

        # Haxe defines (e.g -D something)
        self.defines       = kwargs.get('defines', [])
