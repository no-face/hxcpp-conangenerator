#!/usr/bin/env python
# -*- coding: utf-8 -*-

class Define(object):
    def __init__(self, name, value=None, **kwargs):
        self.name = name
        self.value = value
        self.unset = kwargs.get('unset', False)

    def __eq__(self, other):
        try:
            return self.name == other.name \
               and self.value == other.value
        except:
            return False;

    def __ne__(self, other):
        return not self.__eq__(other)
