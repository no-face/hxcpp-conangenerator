#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans.client.build.compiler_flags import *

from .DepsInfo import DepsInfo
from .Define import Define

import re

class DepsInfoBuilder(object):
    def __init__(self, **kwargs):
        pass

    def extract(self, conanfile, deps_cpp_info):
        info = DepsInfo()

        settings = conanfile.settings
        compiler = settings.get_safe("compiler")
        libcxx   = settings.get_safe("compiler.libcxx")
        os       = settings.get_safe("os")
        os_build = settings.get_safe("os_build") or os
        arch     = settings.get_safe("arch")

        info.compilerflags = deps_cpp_info.cppflags
        info.compilerflags += format_include_paths(deps_cpp_info.include_paths, compiler=compiler)
        info.compilerflags += format_defines(deps_cpp_info.defines)
        info.compilerflags += self._libcxx_compiler_flags(compiler, libcxx)
        info.compilerflags += self.arch_flags(conanfile, compiler)

        info.linkflags     = deps_cpp_info.sharedlinkflags
        info.linkflags    += self._libcxx_link_flags(compiler, libcxx)

        info.rpath_flags = self.fixed_rpath_flags(os_build, compiler, deps_cpp_info.lib_paths)

        info.libpaths      = deps_cpp_info.lib_paths
        info.libs          = format_libraries(deps_cpp_info.libs             , compiler=compiler)

        info.defines       = self.get_arch_defines(arch)
        info.defines      += self.get_runtime_defines(compiler, settings.get_safe("compiler.runtime"))

        return info

    def arch_flags(self, conanfile, compiler):
        arch = conanfile.settings.get_safe("arch")
        arch_flag = architecture_flag(compiler=compiler, arch=arch)

        return [arch_flag] if arch_flag else []

    def get_arch_defines(self, arch):
        return {
            'x86'   : [ Define('HXCPP_M32'), Define('HXCPP_M64', unset=True) ],
            'x86_64': [ Define('HXCPP_M64'), Define('HXCPP_M32', unset=True) ]
        }.get(arch, [])

    def get_runtime_defines(self, compiler, runtime):
        if runtime is None or compiler != "Visual Studio":
            return []

        return [Define("ABI", "-{0}".format(runtime))]

    def fixed_rpath_flags(self, os_build, compiler, lib_paths):
        flags = rpath_flags(os_build, compiler, lib_paths)

        # hxcpp command does not work if flags are quoted
        return [self.remove_quotes(f) for f in flags]

    def remove_quotes(self, text):

        def unquote(match):
            return match.group(1)

        return re.sub(r'"([^"]*)"', unquote, text)

    def _libcxx_compiler_flags(self, compiler, libcxx):
        if not libcxx:
            return []

        stdlib_define = libcxx_define(compiler=compiler, libcxx=libcxx)

        return format_defines([stdlib_define]) + self._libcxx_link_flags(compiler, libcxx)

    def _libcxx_link_flags(self, compiler, libcxx):
        if not libcxx:
            return []

        cxxf = libcxx_flag(compiler=compiler, libcxx=libcxx)
        return [cxxf] if cxxf else []
