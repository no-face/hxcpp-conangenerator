#!/usr/bin/env python
# -*- coding: utf-8 -*-

import xml.etree.cElementTree as ET
from xml.etree.cElementTree import TreeBuilder

class DependenciesWriter(object):

    def __init__(self):

        self.builder = TreeBuilder()
        self.root = self.builder.start("xml")
        self.elements = []

    def write_dependencies(self, depsInfo):
        self.write_defines(depsInfo)
        self.write_flags_for_file(depsInfo)
        self.write_target_info(depsInfo)

        return self.dumpXml()

    ###################################

    def write_defines(self, depsInfo):
        for d in depsInfo.defines:
            define_tag = "unset" if d.unset else "set"

            self.simple_tag(define_tag, self.define_args(d))

    def define_args(self, define):
        args = {
            "name": define.name
        }

        if not define.unset:
            args["value"] = define.value if define.value is not None else "1"

        return args

    def write_flags_for_file(self, depsInfo):
        for groupfile in ['__externs__', 'haxe']:
            self.write_compiler_flags(groupfile, depsInfo)


    def write_compiler_flags(self, groupfile, depsInfo):
        if not depsInfo.compilerflags:
            return

        self.open_tag('files', {
            "id": groupfile
        })

        for flag in depsInfo.compilerflags:
            self.simple_tag("compilerflag", {
                "value": flag
            })

        self.close_tag()

    def write_target_info(self, depsInfo, targets=None):
        if not self.has_target_info(depsInfo):
            return

        targets = targets or ['haxe']

        for t in targets:
            self.open_tag('target', {"id": t})
            self.write_linker_flags(depsInfo)
            self.write_libs(depsInfo)
            self.write_libpaths(depsInfo)
            self.write_rpaths(depsInfo)
            self.close_tag()

    def has_target_info(self, depsInfo):
        return depsInfo.linkflags or depsInfo.libs or depsInfo.libpaths or depsInfo.rpath_flags

    def write_linker_flags(self, depsInfo):
        for flag in depsInfo.linkflags:
            self.simple_tag('flag', {'value': flag})

    def write_libs(self, depsInfo):
        for lib in depsInfo.libs:
            self.simple_tag("lib", {'name': lib, 'unless': 'static_link'})

    def write_libpaths(self, depsInfo):
        for path in depsInfo.libpaths:
            self.simple_tag("libpath", {'name': path, 'unless': 'static_link'})

    def write_rpaths(self, depsInfo):
        for flag in depsInfo.rpath_flags:
            self.simple_tag("flag", {'value': flag, 'if': 'use_rpath'})

    def simple_tag(self, tag, attrs=None):
        self.open_tag(tag, attrs)
        self.close_tag()

    def open_tag(self, tag, attrs=None):
        self.builder.start(tag, attrs)
        self.elements.append(tag)

    def close_tag(self):
        tag = self.elements.pop()
        self.builder.end(tag)

    def dumpXml(self):
        self.indent(self.root)

        return ET.tostring(self.root)

    def indent(self, elem, level=0, ident_char="    "):
        i = "\n" + level*ident_char
        if len(elem):
            if not elem.text or not elem.text.strip():
                elem.text = i + ident_char
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
            for elem in elem:
                self.indent(elem, level+1)
            if not elem.tail or not elem.tail.strip():
                elem.tail = i
        else:
            if level and (not elem.tail or not elem.tail.strip()):
                elem.tail = i
