#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans.model import Generator

from .DepsInfoBuilder import DepsInfoBuilder
from .DependenciesWriter import DependenciesWriter

class HxcppGenerator(Generator):
    def __init__(self, conanfile):
        super(HxcppGenerator, self).__init__(conanfile)

        self.conanfile = conanfile
        self.depsBuilder = DepsInfoBuilder()
        self.depsWriter = DependenciesWriter()

    @property
    def filename(self):
        return "conanbuildinfo_hxcpp.xml"

    @property
    def content(self):
        deps = self.depsBuilder.extract(self.conanfile, self.conanfile.deps_cpp_info)

        return self.depsWriter.write_dependencies(deps)
