#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Conan Generator for haxe (hxcpp)
"""

from conans import ConanFile

class Recipe(ConanFile):
    name        = "HxcppGenerator"
    version     = "0.1.4"
    description = "Conan generator for hxcpp projects."
    url         = "https://gitlab.com/no-face/hxcpp-conangenerator"
    license     = "Unlicense"
    topics      = ("haxe", "generator")
    exports     = "hxcpp_generator/*.py", "__init__.py", "UNLICENSE.md"
    build_policy = "missing"

    settings    = "arch"

    def build(self):
        pass

    def package(self):
        pass

    def package_id(self):
        # Settings should not change the package (only the env info)
        self.info.settings.arch = ''

    def package_info(self):
        self.cpp_info.includedirs = []
        self.cpp_info.libdirs = []
        self.cpp_info.bindirs = []

        # Sets the required arch defines (fix issues with Visual Studio)
        if self.settings.arch == "x86":
            self.env_info.HXCPP_M32 = "1"
        elif self.settings.arch == "x86_64":
            self.env_info.HXCPP_M64 = "1"

from hxcpp_generator import HxcppGenerator

class Hxcpp(HxcppGenerator):
    pass
