#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans.model.build_info import DepsCppInfo, CppInfo
from conans.model.settings import Settings
from conans.model.options import PackageOptions, Options

import os

SETTINGS_DEFINITION = {
    "compiler" : {
        "gcc" : {
            "libcxx" : ["libstdc++", "libstdc++11"]
        },
        "clang" : {
            "libcxx" : ["libstdc++", "libstdc++11", "libc++"]
        },
        "Visual Studio": {
            "runtime": ["MD", "MT", "MTd", "MDd"]
        }
    },
    "os": ["Windows", "Linux", "Macos", "Android"],
    "os_build": "ANY",
    "arch" : ["x86", "x86_64"]
}

class FakeConanFile(object):
    def __init__(self, settings_def=SETTINGS_DEFINITION):
        self.deps_env_info  = {}
        self.env_info       = {}
        self.deps_cpp_info  = DepsCppInfo()
        self.cpp_info       = {}
        self.deps_user_info = {}

        self.settings = Settings(definition=settings_def)
        self.options  = Options(PackageOptions.loads("{}"))


class Store(object):
    '''Class used to allow store arbitrary properties'''

    def append_value(self, key, values):
        if not hasattr(self, key):
            setattr(self, key, [])

        getattr(self, key).extend(values)

class FakeContext(object):
    def __init__(self):
        self.env = Store()

class FakedCppInfo(CppInfo):
    def __init__(self, *k, **kwargs):
        super(FakedCppInfo,self).__init__(*k, **kwargs)

    def _filter_paths(self, paths):
        abs_paths = [os.path.join(self.rootpath, p)
                    if not os.path.isabs(p) else p for p in paths]
        return abs_paths

class DepsBuilder(object):
    def __init__(self, conanfile, deps):
        self.conanfile = conanfile
        self.deps = deps

    def add_dependency(self, dep_name):
        self.conanfile.deps_cpp_info.update(self.deps[dep_name], dep_name)

    def build(self):
        return self.conanfile.deps_cpp_info

def build_cpp_info(base_dir, **kw):
    cpp_info = FakedCppInfo(base_dir)

    for attr, value in kw.items():
        setattr(cpp_info, attr, value)

    return cpp_info
