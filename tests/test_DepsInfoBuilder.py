#!/usr/bin/env python
# -*- coding: utf-8 -*-

from hxcpp_generator import DepsInfo, DepsInfoBuilder, Define

from .utils import fakes
from .utils.fakes import *

from os import path

import pytest

class TestDepsInfoBuilder:
    def setup_method(self, method):
        steps = DepsInfoBuilderSteps()
        self.steps = steps
        self.given = steps
        self.when = steps
        self.then = steps

    @pytest.mark.parametrize("compiler, include_paths, lib_paths, libs", [
        ("gcc"          , ['-Ifoo_dir/include'] , [path.join('foo_dir', 'lib')], ['-lfoo']),
        ("clang"        , ['-Ifoo_dir/include'] , [path.join('foo_dir', 'lib')], ['-lfoo']),
        ("Visual Studio", ['-Ifoo_dir\\include'], [path.join('foo_dir', 'lib')], ['foo.lib'])
    ])
    def test_extract_info_from_simple_dependency(self, compiler, include_paths, lib_paths, libs):
        self.given.i_am_using_these_settings({
            'compiler': compiler
        })
        self.given.this_project_depends_of({
            'foo': build_cpp_info('foo_dir',
                libs=['foo']
            )
        })
        self.when.extracting_these_dependencies()
        self.then.the_extracted_dependencies_info_should_contain_these_flags({
            'compilerflags': include_paths,
            'libpaths'     : lib_paths,
            'libs'         : libs
        })

    def test_extract_dependencies_flags(self):
        self.given.i_am_using_these_settings({
            'compiler': 'gcc'
        })
        self.given.this_project_depends_of({
            'foo': build_cpp_info('foo_dir',
                defines=['FOO'],
                cppflags=['-std=c++11']
            ),
            'bar': build_cpp_info('bar_dir',
                defines=['EXAMPLE'],
                sharedlinkflags=['-pie']
            )
        })
        self.when.extracting_these_dependencies()
        self.then.the_extracted_dependencies_info_should_contain_these_flags({
            'compilerflags': ['-std=c++11', '-DEXAMPLE', '-DFOO'],
            'linkflags'    : ['-pie']
        })

    @pytest.mark.parametrize("compiler,stdlib, compiler_flags,linkflags", [
        ("gcc"  , "libstdc++"  , ["-D_GLIBCXX_USE_CXX11_ABI=0"], []),
        ("gcc"  , "libstdc++11", ["-D_GLIBCXX_USE_CXX11_ABI=1"], []),
        ("clang", "libstdc++"  , ["-D_GLIBCXX_USE_CXX11_ABI=0", "-stdlib=libstdc++"], ["-stdlib=libstdc++"]),
        ("clang", "libstdc++11", ["-D_GLIBCXX_USE_CXX11_ABI=1", "-stdlib=libstdc++"], ["-stdlib=libstdc++"]),
    ])
    def test_extract_stdlib_flags(self, compiler, stdlib, compiler_flags, linkflags):
        self.given.i_am_using_these_settings({
            'compiler': compiler,
            'compiler.libcxx': stdlib
        })
        self.when.extracting_these_dependencies()
        self.then.the_extracted_dependencies_info_should_contain_these_flags({
            'compilerflags': compiler_flags,
            'linkflags': linkflags
        })

    @pytest.mark.parametrize("compiler,arch,defines,archflags", [
        ("gcc"  , "x86"   , [Define("HXCPP_M32"), Define("HXCPP_M64", unset=True)], ["-m32"]),
        ("gcc"  , "x86_64", [Define("HXCPP_M64"), Define("HXCPP_M32", unset=True)], ["-m64"]),
        ("clang", "x86"   , [Define("HXCPP_M32"), Define("HXCPP_M64", unset=True)], ["-m32"]),
        ("clang", "x86_64", [Define("HXCPP_M64"), Define("HXCPP_M32", unset=True)], ["-m64"])
    ])
    def test_extract_arch_flag(self, compiler, arch, defines, archflags):
        self.given.i_am_using_these_settings({
            'compiler': compiler,
            'arch': arch
        })
        self.when.extracting_these_dependencies()
        self.then.the_extracted_dependencies_info_should_contain_these_flags({
            'compilerflags': archflags,
            'defines'      : defines
        })
        self.then.the_extracted_dependencies_info_should_NOT_contain_these_flags({
            'linkflags': archflags
        })


    @pytest.mark.parametrize("compiler, os, os_build, rpath_flag, rpaths", [
        ("gcc"  , "Linux"   , None   , "-Wl,-rpath={0}", ["foo_dir/lib"]),
        ("clang", "Windows" , None   , "-Wl,-rpath={0}", ["foo_dir/lib"]),
        ("gcc"  , "Macos"   , "Linux", "-Wl,-rpath={0}", ["foo_dir/lib"]),
        ("clang", "Macos"   , None   , "-Wl,-rpath,{0}", ["foo_dir/lib"]),
        ("gcc"  , "Linux"   , "Macos", "-Wl,-rpath,{0}", ["foo_dir/lib"]),
        ("Visual Studio", "Windows", None     , "", []),
        ("Visual Studio", "Linux"  , "Windows", "", []),
        ("Visual Studio", "Linux"  , None     , "", []),
    ])
    def test_extract_rpath_flag(self, compiler, os, os_build, rpath_flag, rpaths):
        self.given.i_am_using_these_settings({
            'compiler': compiler,
            'os': os,
            'os_build': os_build
        })
        self.given.this_project_depends_of({
            'foo': build_cpp_info('foo_dir',
                libs=['foo']
            )
        })
        self.when.extracting_these_dependencies()
        self.then.the_extracted_dependencies_info_should_contain_these_flags({
            'rpath_flags': [rpath_flag.format(p) for p in rpaths],
        })

    @pytest.mark.parametrize("runtime", [
        ("MT"),
        ("MT"),
        ("MTd"),
        ("MDd"),
    ])
    def test_extract_visual_studio_runtime_flags(self, runtime):
        self.given.i_am_using_these_settings({
            'compiler': "Visual Studio",
            'compiler.runtime': runtime
        })
        self.when.extracting_these_dependencies()
        self.then.the_extracted_dependencies_info_should_contain_these_flags({
            'defines': [Define("ABI", '-'+runtime)],
        })

class DepsInfoBuilderSteps:
    def __init__(self):
        self.conanfile = FakeConanFile()
        self.builder = DepsInfoBuilder()
        self.deps_cpp_info = self.conanfile.deps_cpp_info
        self.deps_info = None

    @property
    def lib_paths(self):
        return [str(p) for p in self.deps_cpp_info.lib_paths]

    def i_am_using_these_settings(self, settings):
        values_list = []

        for name in sorted(settings.keys()):
            values_list.append((name, settings[name]))

        self.conanfile.settings.values_list = values_list

    def this_project_depends_of(self, deps):
        for dep_name, dep in deps.items():
            self.deps_cpp_info.update(dep, dep_name)

    def extracting_these_dependencies(self):
        self.deps_info = self.builder.extract(self.conanfile, self.deps_cpp_info)

    def the_extracted_dependencies_info_should_contain_these_flags(self, expected):
        self.check_contains_flags(expected, True)

    def the_extracted_dependencies_info_should_NOT_contain_these_flags(self, expected):
        self.check_contains_flags(expected, False)

    def adjust_path(self, p, compiler):
        from conans.client.build import compiler_flags

        return compiler_flags.adjust_path(p, compiler=compiler)

    def check_contains_flags(self, flags, contains):
        assert self.deps_info is not None

        for attr_name in flags:
            print(attr_name)

            extracted_flags = getattr(self.deps_info, attr_name)

            for f in flags.get(attr_name, []):
                if contains:
                    assert f in extracted_flags
                else:
                    assert f not in extracted_flags
