#!/usr/bin/env python
# -*- coding: utf-8 -*-

from hxcpp_generator import HxcppGenerator, DepsInfo, DepsInfoBuilder, DependenciesWriter

from .utils import fakes

from mock import patch

class TestGenerator:

    def setup_method(self, method):
        steps = GeneratorSteps()
        self.given = steps
        self.when = steps
        self.then = steps

    def test_generate_content(self):
        self.given.a_generator_instance_with_a_valid_conanfile()
        self.when.generating_the_file_content()
        self.then.it_should_extract_the_dependencies_info_from_the_conanfile()
        self.then.the_document_content_should_be_generated_from_the_extracted_dependencies()
        self.then.the_generator_should_return_the_writed_content()

class GeneratorSteps(object):

    def __init__(self):
        self.generator = None
        self.conanfile = fakes.FakeConanFile()
        self.generated_content = None
        self.expected_content = "Lorem ipsum"

        self.mockBuilder = None
        self.mockWriter = None
        self.deps = DepsInfo()

    @patch('hxcpp_generator.DepsInfoBuilder')
    @patch('hxcpp_generator.DependenciesWriter')
    def a_generator_instance_with_a_valid_conanfile(self, MockBuilder, MockWriter):
        self.mockBuilder = MockBuilder()
        self.mockBuilder.extract.return_value = self.deps
        self.mockWriter = MockWriter()
        self.mockWriter.write_dependencies.return_value = self.expected_content

        self.generator = HxcppGenerator(self.conanfile)
        self.generator.depsBuilder = self.mockBuilder
        self.generator.depsWriter = self.mockWriter

    def generating_the_file_content(self):
        self.generated_content = self.generator.content

    def it_should_extract_the_dependencies_info_from_the_conanfile(self):
        self.mockBuilder.extract.assert_called_with(self.conanfile, self.conanfile.deps_cpp_info)

    def the_document_content_should_be_generated_from_the_extracted_dependencies(self):
        self.mockWriter.write_dependencies.assert_called_with(self.deps)

    def the_generator_should_return_the_writed_content(self):
        assert self.generated_content is self.expected_content
