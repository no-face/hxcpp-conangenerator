#!/usr/bin/env python
# -*- coding: utf-8 -*-

from hxcpp_generator import DepsInfo, DependenciesWriter, Define


from os import path

class TestWriter:
    def setup_method(self, method):
        steps = WriterSteps()
        self.given = steps
        self.when = steps
        self.then = steps

    def test_write_empty_dependencies(self):
        self.given.these_dependencies_info({})
        self.when.writing_these_dependencies()
        self.then.this_output_is_expected('<xml />')


    def test_write_compiler_flags(self):
        self.given.these_dependencies_info({
            "compilerflags" : [
                '-I{0}'.format(path.join('foo', 'include'))
            ]
        })
        self.when.writing_these_dependencies()
        self.then.this_output_is_expected('''
<xml>
    <files id="__externs__">
        <compilerflag value="{0}" />
    </files>
    <files id="haxe">
        <compilerflag value="{0}" />
    </files>
</xml>
'''.format(
        self.given.deps_info.compilerflags[0]
    ))

    def test_write_link_flags(self):
        self.given.these_dependencies_info({
            "linkflags": [
                '-L{0}'.format(path.join('foo', 'lib'))
            ]
        })
        self.when.writing_these_dependencies()
        self.then.this_output_is_expected('''
<xml>
    <target id="haxe">
        <flag value="{0}" />
    </target>
</xml>
'''.format(
        self.given.deps_info.linkflags[0],
    ))

    def test_write_libs(self):
        self.given.these_dependencies_info({
            "libs": [
                "-lfoo",
                "-lbar",
                "-lbuzz"
            ]
        })
        self.when.writing_these_dependencies()
        self.then.this_output_is_expected('''
<xml>
    <target id="haxe">
        <lib name="{0}" unless="static_link" />
        <lib name="{1}" unless="static_link" />
        <lib name="{2}" unless="static_link" />
    </target>
</xml>
'''.format(
        self.given.deps_info.libs[0],
        self.given.deps_info.libs[1],
        self.given.deps_info.libs[2])
        )

    def test_write_libpaths(self):
        self.given.these_dependencies_info({
            "libpaths": [
                path.join('lib', 'foo'),
                path.join('lib', 'bar'),
                path.join('lib', 'buzz')
            ]
        })
        self.when.writing_these_dependencies()
        self.then.this_output_is_expected('''
<xml>
    <target id="haxe">
        <libpath name="{0}" unless="static_link" />
        <libpath name="{1}" unless="static_link" />
        <libpath name="{2}" unless="static_link" />
    </target>
</xml>
'''.format(
        self.given.deps_info.libpaths[0],
        self.given.deps_info.libpaths[1],
        self.given.deps_info.libpaths[2])
        )


    def test_write_rpath_flags(self):
        self.given.these_dependencies_info({
            "rpath_flags": [
                "-Wl,-rpath='%s'" % path.join('path', 'to', 'foo'),
                "-Wl,-rpath='%s'" % path.join('path', 'to', 'bar'),
                "-Wl,-rpath='%s'" % path.join('path', 'to', 'buzz'),
            ]
        })
        self.when.writing_these_dependencies()
        self.then.this_output_is_expected('''
<xml>
    <target id="haxe">
        <flag if="use_rpath" value="{0}" />
        <flag if="use_rpath" value="{1}" />
        <flag if="use_rpath" value="{2}" />
    </target>
</xml>
'''.format(
        self.given.deps_info.rpath_flags[0],
        self.given.deps_info.rpath_flags[1],
        self.given.deps_info.rpath_flags[2])
        )

    def test_write_defines(self):
        self.given.these_dependencies_info({
            "defines": [
                Define("HXCPP_M32"),
                Define("linux"),
                Define("name",  "value"),
                Define("HXCPP_M64", unset=True)
            ]
        })
        self.when.writing_these_dependencies()
        self.then.this_output_is_expected('''
<xml>
    <set name="{0}" value="1" />
    <set name="{1}" value="1" />
    <set name="{2}" value="{3}" />
    <unset name="{4}" />
</xml>
'''.format(
            self.given.deps_info.defines[0].name,
            self.given.deps_info.defines[1].name,
            self.given.deps_info.defines[2].name,
            self.given.deps_info.defines[2].value,
            self.given.deps_info.defines[3].name
          )
        )

class WriterSteps:
    def __init__(self):
        self.output = None

    def these_dependencies_info(self, deps_info):
        self.deps_info = DepsInfo(**deps_info)

    def dependencies_with_libpaths(self, libs):
        libpaths = [path.join('lib', l) for l in libs]

        self.these_dependencies_info({
            "libpaths": libpaths
        })

    def writing_these_dependencies(self):
        writer = DependenciesWriter()
        out = writer.write_dependencies(self.deps_info)

        assert out is not None

        self.output = out.strip().decode()

    def this_output_is_expected(self, expected):
        assert self.output is not None

        print("output:\n{0}".format(self.output))
        print("------------------------")
        print("expected:\n{0}".format(expected))

        assert self.output == expected.strip()

    def the_output_should_contains(self, expected):
        expected = self.filter_empty_lines(expected)

        print('output:\n' + self.output)
        print('------------------------')
        print('should contains:\n' + expected)

        assert expected in self.output

    def filter_empty_lines(self, text):
        import re

        lines = text.split('\n')
        filtered = filter(lambda x: not re.match(r'^\s*$', x), lines)

        return '\n'.join(filtered)
