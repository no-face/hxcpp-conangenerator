# Install conan package tools
sudo pip install --upgrade conan_package_tools

# Install Haxe from ppa

## Install command to add ppa
sudo apt-get -qq update
sudo apt-get -qq install software-properties-common -y

## Add ppa
sudo add-apt-repository ppa:haxe/releases -y
sudo apt-get -qq update

## Install haxe
sudo apt-get -qq install -y haxe
