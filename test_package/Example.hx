import haxe.extern.Rest;

@:buildXml("<include name='${CONANFILE_DIR}/conanbuildinfo_hxcpp.xml'/>")
class Example
{
  static function main() {
    Fmt.print("************* {} *************\n", "Format example");
  }
}

@:include("fmt/format.h")
extern class Fmt {
  @:native('fmt::print')
  static function print(format: cpp.ConstCharStar, args: Rest<cpp.VarArg>) : Void;
}
