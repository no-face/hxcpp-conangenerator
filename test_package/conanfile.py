#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile, tools
from os import path

class ExampleConanFile(ConanFile):
    build_policy = "missing"
    settings = "os", "compiler", "build_type", "arch"

    generators = "Hxcpp"

    requires = "fmt/5.3.0@bincrafters/stable"

    @property
    def build_path(self):
        return path.join(self.build_folder, 'build')

    def imports(self):
        self.copy("*.dll", dst="bin", src="bin") # From bin to bin
        self.copy("*.dylib*", dst="bin", src="lib") # From lib to bin

    def build(self):
        self.run("haxe -main Example -cpp {0} -D CONANFILE_DIR={1} -D HXCPP_VERBOSE".format(
                self.build_path, self.build_folder), cwd=self.source_folder)

    def test(self):
        if tools.cross_building(self.settings):
            self.output.info("Cross building - skip run test")
            return

        self.run(path.join(self.build_path, "Example"))
